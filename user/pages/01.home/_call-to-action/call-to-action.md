---
title: 'Call To Action'
published: true
taxonomy:
    category:
        - call-to-action
body_classes: ul
process:
    twig: true
    markdown: false
class: small
---

<p>PA 369K advocates for the well-being and learning support for students with special needs. We also provide a platform for parents, teachers and staff to collaborate on initiatives to help every student succeed.</p>
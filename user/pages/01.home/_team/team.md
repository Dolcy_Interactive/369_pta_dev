---
title: Team
published: true
body_classes: team
process:
    markdown: false
    twig: false
---

<ul>
<li><strong>Shaughn Dolcy,</strong> President</li>
<li><strong>Natasha Henry</strong>, Vice President</li>
<li><strong>Karen Chandler</strong>, Secretary</li>
<li><strong>Shana Hayes</strong>, Treasurer</li>
</ul>
---
title: 'Virtual Town Hall Meeting with Chancellor Carranza'
published: true
process:
    twig: true
    markdown: false
---

<p><strong>When:</strong> February 3, 2021</p>
<p>The Citywide Council for District 75 (CCD75) will host a Virtual Town Hall Meeting with Chancellor Carranza on February 3, 2021 at 6:00pm-7:30pm (Chancellor arrives 6:30pm).</p>
<p>Before the town hall meeting, we prepared a questionnaire that we would like for the District 75 parents to submit in advance to ask the Chancellor directly during the meeting. Parents will be chosen in advance by the council to ask their question(s). Please indicate: If a language other than English is required.</p>
<p><a class="btn outline btn-info" title="Click to learn more" href="https://www.facebook.com/CECD75/">Click to learn more</a></p>
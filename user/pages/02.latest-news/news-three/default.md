---
title: 'Join The P369K Remind List'
published: true
---

<p>Using the FREE Remind app, you will get current text notifications for:</p>
<ul>
<li>The latest updates in your school</li>
<li>Information on school closings</li>
<li>Links to FREE activities for your children</li>
<li>Links to FREE seminars and valuable information sessions for the support of your child</li>
<li>Information on school events</li>
<li>Information on community services and events</li>
<li>Information on NYC DOE announcements</li>
<li>Parent association meetings</li>
</ul>
<p>It is simple to do. Find your child&rsquo;s school below and text the appropriate code to the number 81010. That is it!</p>
<p>If you need any assistance, please contact your Parent Coordinator: Sofia Bugdady at 917-330-3911 or <a title="sbugdady@schools.nyc.gov" href="mailto:sbugdady@schools.nyc.gov">sbugdady@schools.nyc.gov</a></p>